package dadata

import (
	"encoding/json"
	"reflect"
)

type LocationRestriction interface{}

type LocationRestrictionIso struct {
	LocationRestriction
	CountryIsoCode string `json:"country_iso_code"`
	RegionIsoCode  string `json:"region_iso_code"`
}

type LocationRestrictionKladr struct {
	LocationRestriction
	KladrId string `json:"kladr_id"`
}

type LocationRestrictionFias struct {
	LocationRestriction
	RegionFiasId     string `json:"region_fias_id"`
	AreaFiasId       string `json:"area_fias_id"`
	CityFiasId       string `json:"city_fias_id"`
	SettlementFiasId string `json:"settlement_fias_id"`
	StreetFiasId     string `json:"street_fias_id"`
}

type LocationRestrictionName struct {
	LocationRestriction
	Region     string `json:"region"`
	Area       string `json:"area"`
	City       string `json:"city"`
	Settlement string `json:"settlement"`
	Street     string `json:"street"`
}

type LocationRestrictionCountry struct {
	LocationRestriction
	Country string `json:"country"`
}

type LocationRestrictionTypeObject struct {
	LocationRestriction
	RegionTypeFull     string `json:"region_type_full"`
	AreaTypeFull       string `json:"area_type_full"`
	CityTypeFull       string `json:"city_type_full"`
	SettlementTypeFull string `json:"settlement_type_full"`
	StreetTypeFull     string `json:"street_type_full"`
}

type LocationsGeo struct {
	LocationRestriction
	Lat          string `json:"Lat"`
	Lon          string `json:"Lon"`
	RadiusMeters string `json:"radius_meters"`
}

type LocationBoost struct {
	KladrId string `json:"kladr_id"`
}

type LocationBound struct {
	Value string `json:"value"`
}

type AddressRequestJson struct {
	Query          string                `json:"query" binding:"required"`
	Count          int                   `json:"count"`
	Locations      []LocationRestriction `json:"locations"`
	LocationsGeo   []LocationsGeo        `json:"locations_geo"`
	LocationsBoost []LocationBoost       `json:"locations_boost"`
	FromBound      LocationBound         `json:"from_bound"`
	ToBound        LocationBound         `json:"to_bound"`
	Language       string                `json:"language"`
}

type AddressRequest struct {
	AddressRequestJson
	locations    []LocationRestriction
	locationsGeo []LocationsGeo
}

func (f *AddressRequest) AddRestriction(restriction LocationRestriction) *AddressRequest {
	if reflect.TypeOf(restriction) == reflect.TypeOf(LocationsGeo{}) {
		f.locationsGeo = append(f.locationsGeo, restriction.(LocationsGeo))
	} else {
		f.locations = append(f.locations, restriction)
	}
	return f
}

func (f *AddressRequest) Locations() []LocationRestriction {
	return f.locations
}

func (f *AddressRequest) LocationsGeo() []LocationsGeo {
	return f.locationsGeo
}

func (f *AddressRequest) MarshalJSON() ([]byte, error) {
	return json.Marshal(AddressRequestJson{
		Query:          f.Query,
		Count:          f.Count,
		Locations:      f.Locations(),
		LocationsGeo:   f.LocationsGeo(),
		LocationsBoost: f.LocationsBoost,
		FromBound:      f.FromBound,
		ToBound:        f.ToBound,
		Language:       f.Language,
	})
}
