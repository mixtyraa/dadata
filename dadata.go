package dadata

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"
)

type Dadata struct {
	Request AddressRequest
	token   string
}

func (f *Dadata) Suggest(q string) (Response, error) {
	var answer Response = Response{}

	f.Request.Query = q
	client := http.Client{}
	bodyRequest, _ := f.Request.MarshalJSON()
	reader := strings.NewReader(string(bodyRequest))
	req, err := http.NewRequest(
		"POST",
		"https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address",
		reader,
	)
	if err != nil {
		return answer, err
	}

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Authorization", "Token "+f.token)
	response, err := client.Do(req)

	bodyResponse, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return answer, err
	}
	err = json.Unmarshal(bodyResponse, &answer)
	return answer, err
}

func (f *Dadata) setRequest(request AddressRequest) *Dadata {
	f.Request = request
	return f
}

func NewDadata(token string) *Dadata {
	return &Dadata{
		token: token,
	}
}
